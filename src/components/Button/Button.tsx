import React, { Component } from 'react';

import styles from './Button.module.scss'

class Button extends Component {
  render() {
    return <button className={styles.colonyButton} >COLONY</button>
  }
}
export default Button; // Don’t forget to use export default!