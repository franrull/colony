import React, { Component } from 'react';

import { getColonyLogs } from '../../providers/colony.provider';

import Event from '../../components/Event/Event';
import styles from './ColonyLogList.module.scss'

class ColonyLogList extends Component<{}, { eventLogs: any }> {
    
    constructor(props: Readonly<{}>) {
        super(props);
        this.state = { eventLogs: []};
    }

    async componentDidMount() {
        
        try {
            await getColonyLogs().then(eventLogs => {
                this.setState({ eventLogs });
            });
        }
        catch (e) {
            console.error(e);
        }
        
    }

    logList = (eventLogs: any[]) => {
        return (
            <section>
                {eventLogs.map((log, index) => (
                    <Event key={index} log={log}></Event>
                ))}
            </section>
        );
    }

    render() {
        const { eventLogs } = this.state;

        return (
            <div className={styles.wrapper}>
                <div className={styles.colonyList}>
                    {eventLogs.length > 0 ? this.logList(eventLogs) : 'Loading Logs'}
                </div>
            </div>
        );
    }
}
export default ColonyLogList; // Don’t forget to use export default!