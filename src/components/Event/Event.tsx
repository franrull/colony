import React, { Component } from 'react';
import moment from 'moment';
import { decodeAmount, decodeToken, decodeId, decodeRole, getAvatar } from '../../providers/colony.provider';

import styles from './Event.module.scss'

class Event extends Component<{ log: any }, { avatar: string, date: string, primaryText: JSX.Element | null }> {
    
    constructor(props: Readonly<{ log: any, primaryText: JSX.Element }>) {
        super(props);
        this.state = { avatar: '', date: '... loading date', primaryText: null };
    }

    componentDidMount() {
        const { props: { log } } = this;
        try {
            this.getAvatar();
            this.getDate();
            this.getLogPrimaryText();
        }
        catch (e) {
            console.error(e);
        }
    }

    getAvatar() {
        const { props: {log} } = this;
        getAvatar(log).then(avatar => {
            this.setState({ avatar })
        });
    }

    getDate() {
        const { props: { log } } = this;
        const date = moment(log.date).format('D MMM')
        this.setState({ date });
    }

    getLogPrimaryText() {
        
        const { props: { log } } = this;
        
        let primaryText = null;
        switch (log.name) {
            case 'PayoutClaimed':
                primaryText = (<span>User <b>{[log.address]}</b> claimed <b>{decodeAmount(log.amount)} {decodeToken(log.token)}</b> payout from pot <b>{decodeId(log.fundingPotId)}</b>.</span>)
                break;
            case 'ColonyRoleSet':
                primaryText = (<span><b>{decodeRole(log.role)}</b> role assigned to user <b>{[log.user]}</b> in domain <b>{decodeId(log.domainId)}</b>.</span>)
                break;
            case 'ColonyInitialised':
                primaryText = (<span>Congratulations! It's a beautiful baby colony!</span>)
                break;
            case 'DomainAdded':
                primaryText = (<span>Domain <b>{decodeId(log.domainId)}</b> added.</span>)
                break;
        }
        
        this.setState({ primaryText })
    }

    render () {
        const { avatar, date, primaryText } = this.state;
        
        return (
            <div className={styles.event}>
                <div className={styles.avatar}>
                    {avatar ? (<img src={avatar} alt="" />) : ''}
                </div>
                <div className={styles.logInfo}>
                    <div className={styles.primaryText}>{primaryText}</div>
                    <div className={styles.logDate}>{date}</div>
                </div>
            </div>
        )
    }
}
export default Event; // Don’t forget to use export default!
