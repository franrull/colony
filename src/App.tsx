import React from 'react';

import ColonyLogList from './components/ColonyLogList/ColonyLogList';

import './App.module.scss';

function App() {
  return (
    <ColonyLogList></ColonyLogList>
  );
}

export default App;