import axios from "axios";
import makeBlockie from 'ethereum-blockies-base64';
import { getColonyNetworkClient, getLogs, Network, ColonyClientV2, getBlockTime, ColonyRole } from '@colony/colony-js';
import { Wallet, utils } from 'ethers';
import { InfuraProvider } from 'ethers/providers';
import { BigNumber } from 'ethers/utils';

// Set up the network address constants that you'll be using
// The two below represent the current ones on mainnet
// Don't worry too much about them, just use them as-is
const MAINNET_NETWORK_ADDRESS = `0x5346D0f80e2816FaD329F2c140c870ffc3c3E2Ef`;
const MAINNET_BETACOLONY_ADDRESS = `0x869814034d96544f3C62DE2aC22448ed79Ac8e70`;

// Get a new Infura provider (don't worry too much about this)
let provider = new InfuraProvider();

// import api key from the env variable
const apiKey = `A36F3B2VNVA3195QUUK1ARG7FQMIRVDDD2`;

export let colonyClient: ColonyClientV2;
export let symbols: any[] = [];

export const initColonyList = () => {

    // Get a random wallet
    // You don't really need control over it, since you won't be firing any trasactions out of it
    const wallet = Wallet.createRandom();
    // Connect your wallet to the provider
    const connectedWallet = wallet.connect(provider);

    // Get a network client instance
    let networkClient = getColonyNetworkClient(
        Network.Mainnet,
        connectedWallet,
        { networkAddress: MAINNET_NETWORK_ADDRESS },
    )

    // Get the colony client instance for the betacolony
    return networkClient.getColonyClient(MAINNET_BETACOLONY_ADDRESS)

}

export const getColonyLogs = () => {

    return initColonyList().then(client => {
        colonyClient = client as ColonyClientV2;

        // Get the filter
        // There's a corresponding filter method for all event types
        let filterPayoutClaimed = colonyClient.filters.PayoutClaimed(null, null, null);
        let filterColonyRoleSet = colonyClient.filters.ColonyRoleSet(null, null, null, null);
        let filterColonyInitialised = colonyClient.filters.ColonyInitialised(null, null);
        let filterDomainAdded = colonyClient.filters.DomainAdded(null);

        return Promise.all([
            getRawLogs(colonyClient, filterPayoutClaimed),
            getRawLogs(colonyClient, filterColonyRoleSet),
            getRawLogs(colonyClient, filterColonyInitialised),
            getRawLogs(colonyClient, filterDomainAdded),
        ]).then(function (allLogs) {
            let rawLogs = allLogs[0].concat(allLogs[1], allLogs[2], allLogs[3]);
            let datePromises: any = [];
            rawLogs.map((log: any) => {
                datePromises.push(getLogDate(log.blockHash))
                return log;
            });

            return Promise.all(datePromises)
                .then(dates => {
                    let formattedLogs = dates.map((date, index) => {
                        
                        let parsedLog = colonyClient.interface.parseLog(rawLogs[index]);

                        const { name, values: { user, token, amount, role, domainId, fundingPotId} } = parsedLog;

                        return {
                            name, token, user, address: rawLogs[index].address, amount, role, domainId, fundingPotId, date
                        }
                        
                    });
                    const orderedLogs = formattedLogs.sort(compareLogs);

                    // Since the token symbols depend on the logs
                    // I needed to chain the promises and delay the logs reponse 
                    // until the symbols were fetched from EtherScan
                    return getAllSymbols(orderedLogs).then(logSymbols => {
                        symbols = logSymbols;
                        return orderedLogs
                    });
                    
                });

        });
    });
}

export const getEtherscanSymbol = (token: string) => {
    const endpoint = `https://api.etherscan.io/api`;
    try {
        return axios
            .get(`${endpoint}?module=account&action=tokentx&startblock=latest&page=1&offset=1&contractaddress=${token}&apikey=${apiKey}`)
            .then(response => {
                if (response.data && response.data.result && response.data.result[0] && response.data.result[0].tokenSymbol) {
                    return { address: token, symbol: response.data.result[0].tokenSymbol }
                }
            });
    }
    catch (e) {
        console.error(e);
    }
}

export const getLogDate = (blockHash: any): Promise<number> => {
    return getBlockTime(provider, blockHash);
}

export const getRawLogs = (colonyClient: ColonyClientV2, filter: any) => {
    // Get the raw logs array
    return getLogs(colonyClient, filter).then((eventLogs) => {
        return eventLogs;
    });
}

export const compareLogs = (a: any, b: any) => {
    if (a.date > b.date) return -1;
    if (b.date > a.date) return 1;
    return 0;
}

export const getAllSymbols = (eventLogs: any[]) => {
    const uniqueTokens = getContractTokens(eventLogs);

    let symbolPromises: any[] = [];
    uniqueTokens.forEach(token => {
        symbolPromises.push(getEtherscanSymbol(token));
    })

    return Promise.all(symbolPromises).then(symbols => {
        return symbols;
    });
}

export const getContractTokens = (eventLogs: any[]) => {
    let tokens: string[] = [];
    eventLogs.map(log => {
        if (log.token && tokens.indexOf(log.token) === -1) {
            tokens.push(log.token);
        }
        return null;
    });
    return tokens;
}

export const decodeAmount = (number: BigNumber) => {
    let amount = new utils.BigNumber(number);
    const wei = new utils.BigNumber(10);
    const convertedAmount = amount.div(wei.pow(18));
    return convertedAmount.toString();
}

export const decodeId = (number: BigNumber) => {
    let amount = new utils.BigNumber(number);
    return amount.toString();
}

export const decodeToken = (token: string) => {
    if (!symbols) return;
    let s = symbols.find(s => s.address === token);
    return s ? s.symbol : '';
}

export const decodeRole = (roleId: number) => {
    return ColonyRole[roleId];
}

export const getAvatar = (log: any) => {
    if (log.name === 'PayoutClaimed') {
        const humanReadableFundingPotId = new utils.BigNumber(log.fundingPotId).toString();

        return colonyClient.getFundingPot(humanReadableFundingPotId).then(({ associatedTypeId }) => {
            return colonyClient.getPayment(associatedTypeId).then(reponse => {
                const avatar = makeBlockie(reponse.recipient);
                return avatar;
            });
        });
    }
    else {
        const avatarBase = log.user ? log.user : log.address;
        const avatar = makeBlockie(avatarBase);
        return Promise.resolve(avatar);
    }
}